package com.rabbitmq.config;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
@Entity
@Table(name="records")
public class RecordsEntity {
	@Id
	private UUID id;
	
	
	@Column(name="BTSListItems",nullable= true)
	@Type(type="text")
	private String BTSListItems;
	
	
	@Column(name="ocr_result", nullable = true)
	@Type(type = "text")
	private String ocr_result;
	
	
	@Column(name="nlp_response",nullable=true)
	@Type(type="text")
	private String nlp_response;
	
	
	@Column(name="csr_search_term", nullable = true)
	@Type(type = "text")
	private String csr_search_term;

	@Column(name="prod_sku", nullable = true)
	@Type(type = "text")
	private String prod_sku;
	
	
	@Column(name="quantity", nullable = true)
	@Type(type = "integer")
	private int quantity;
	
	@Column(name="removed", nullable = true)
	@Type(type="yes_no")
	private boolean removed;
	
	
	
	@Column(name="position",nullable=true)
	@Type(type="integer")
	private int position;

	
}
