package com.rabbitmq.config;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

public interface RecordsReprositry extends CrudRepository<RecordsEntity, Long>{
	public RecordsEntity findById(UUID id);
	
}
