package com.rabbitmq.rabbitfiles;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import com.rabbitmq.config.Configuration;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * Represents a connection with a queue
 * @author syntax
 *
 */

public abstract class EndPointController {
	protected Channel channel;
    protected Connection connection;
    protected String endPointName;
	
    public EndPointController(String endpointName) throws IOException{
         this.endPointName = endpointName;
		
    	 ConnectionFactory factory = new ConnectionFactory();
    	 factory.setUsername(Configuration.USERNAME);
    	 factory.setPassword(Configuration.PASSWORD);
    	 factory.setHost(Configuration.HOSTNAME);
    	 factory.setPort(Configuration.PORT);
    	 Connection conn;
 
    	 try {
    	 conn = factory.newConnection();
	    
         //creating a channel
         channel = connection.createChannel();
	    
         //declaring a queue for this channel. If queue does not exist,
         //it will be created on the server.
         channel.queueDeclare(endpointName, false, false, false, null);
    	 } catch (IOException | TimeoutException e) {
    		 e.printStackTrace();
    	 }
    }
    
    
	
    /**
     * Close channel and connection. Not necessary as it happens implicitly any way. 
     * @throws IOException
     */
     public void close() throws IOException{
         try {
			this.channel.close();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
         this.connection.close();
     }
}
