package com.rabbitmqservice.rabbitmqservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.rabbitmq.config.RecordsEntity;
import com.rabbitmq.config.RecordsReprositry;
import com.rabbitmq.rabbitfiles.NewTaskController;
import com.rabbitmq.rabbitfiles.WorkerController;


@RestController
public class RabbitMqController {
//	@Autowired
//	private RecordsReprositry recordsReprositry;
// 
	@RequestMapping(name="/postrabbitmq", method=RequestMethod.POST)
	public String makeRabbitMQ() throws Exception
	{
		
		 // producing some messages
		 for (int i = 1; i < 20; i++) {
		 final String message = "This is message number " + i;
		 NewTaskController producer = new NewTaskController(message);
		 new Thread(producer).start();
		 }
//		 
//		 Thread consumerThread = new Thread(new WorkerController());
//		 consumerThread.start();
		
		 
	
//	
//		Records lineTable = new Records();
//		lineTable.setId("222");
//		lineTable.setBTSListItems("asdasdas");
//		lineTable.setNlp_response("asdasdas");
//		lineTable.setOcr_result("asdasdas");
//		lineTable.setPosition(3333);
//		lineTable.setProd_sku("saasdad");
//		lineTable.setRemoved(true);
//		lineTable.setQuantity(333);
//		lineTable.setCsr_search_term("asdasd");
	//	recordsReprositry.save(lineTable);
		System.out.println("Saved babe");

		return "Making RabbitMQ";
	}
	
	
	@RequestMapping(name="/getrabbitmq",method=RequestMethod.GET)
	public String getRabbitMQ() throws Exception
	{
		 Thread consumerThread = new Thread(new WorkerController());
		 consumerThread.start();
		
		
		return "Returing RabbitMQ";
	}

}
